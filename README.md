Created using Unity 5.6

### CONTROLS ###

* Drag to move camera
* Click on planets to zoom in and display their orbits
* Use mouse scroll to zoom camera in and out (pinch on mobile)