﻿using System;

public interface ITimeScaleProvider
{
    event EventHandler Changed;
    float TimeScale { get; set; }
}
