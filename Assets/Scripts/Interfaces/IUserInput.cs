﻿using UnityEngine;
using System;

public class DraggingEventArgs : EventArgs
{
    public Vector3 deltaPosition;
}

public class DragEndedEventArgs : EventArgs
{
    public Vector3 position;
}

public interface IUserInput
{
    event EventHandler<DraggingEventArgs> Dragging;
    event EventHandler<DragEndedEventArgs> DragEnded;
    event EventHandler DeselectAll;
}

[Serializable]
public class IUserInputContainer : IUnifiedContainer<IUserInput> {}
