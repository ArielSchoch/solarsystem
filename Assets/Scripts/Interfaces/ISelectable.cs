﻿using UnityEngine;
using System;

public interface  ISelectable
{
    Transform Transform { get; }
    float FocusDistance { get; }
}

[Serializable]
public class ISelectableContainer : IUnifiedContainer<ISelectable> {}
