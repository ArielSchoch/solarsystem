﻿using System;

public interface ITargetFocusSystem
{
    event EventHandler<SelectionEventArgs> Focused;
    void FocusOnTarget(ISelectable target);
}

[Serializable]
public class ITargetFocusSystemContainer : IUnifiedContainer<ITargetFocusSystem> {}
