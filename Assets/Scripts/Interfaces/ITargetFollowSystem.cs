﻿using System;
using UnityEngine;

public interface ITargetFollowSystem
{
    Transform Target { get; set; }
    bool FollowTarget { get; set; }
}

[Serializable]
public class ITargetFollowSystemContainer : IUnifiedContainer<ITargetFollowSystem> {}
