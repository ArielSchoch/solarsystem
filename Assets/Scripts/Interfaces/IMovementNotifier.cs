
public interface IMovementNotifier
{
    event System.EventHandler<MovementEventArgs> OnMoved;
}

[System.Serializable]
public class IMovementNotifierContainer : IUnifiedContainer<IMovementNotifier> {}
