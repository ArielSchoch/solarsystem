using System;
using UnityEngine;

public interface IPanSystem
{
    event EventHandler BeganPan;
    void OnBeganPan();
    void Pan(Vector2 direction);
}

[Serializable]
public class IPanSystemContainer : IUnifiedContainer<IPanSystem> {}
