using System;
using UnityEngine;

public interface IZoomSystem
{
    void Zoom(float zoomAmount, Vector3 screenPoint);
}

[Serializable]
public class IZoomSystemContainer : IUnifiedContainer<IZoomSystem> {}
