﻿using UnityEngine;

public enum SpaceObjectType { Planet, Moon, Star, Comet }

public class SpaceObject : MonoBehaviour
{
    [SerializeField] protected SpaceObjectType type;
    [SerializeField] protected float mass = 1f;
    [SerializeField] protected float diameter = 1f;
    [SerializeField] protected bool solidSurface = true;
    [SerializeField] protected float minTemperature = -20f;
    [SerializeField] protected float maxTemperature = +40f;
}
