﻿using System;
using UnityEngine;

public class GameTimeScale : MonoBehaviour, ITimeScaleProvider
{
    public event EventHandler Changed;
    public float TimeScale { get { return timeScale; } set { timeScale = value; OnChanged(); } }
    [SerializeField] private float timeScale = 1f;

    protected virtual void OnChanged()
    {
        if (Changed != null)
            Changed(this, EventArgs.Empty);
    }
}
