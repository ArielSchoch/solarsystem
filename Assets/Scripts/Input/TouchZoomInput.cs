﻿using UnityEngine;

/*
SUMMARY:
• OnZoom() sends zoomSystem the delta zoom each frame
*/

public class TouchZoomInput : PlatformDependentBehaviour
{
    private const float Half = 0.5f;
    [SerializeField] private IZoomSystemContainer zoomSystem;
    [SerializeField] private float cameraDistance = 10f;
    [SerializeField] private float zoomSpeed = 100f;
    [SerializeField] private bool inverseZoom = false;
    private bool couldBeZoom = false;
    private Vector2 startPos1;
    private Vector2 startPos2;
    private Vector3 screenPoint;
    private float previousDistance;
    private float deltaDistance;
    
    private void Update()
    {
        if (Input.touchCount == 2)
        {
            if (!couldBeZoom)
            {
                startPos1 = Input.touches[0].position;
                startPos2 = Input.touches[1].position;
                // Find the mid point between the two touches
                Vector2 midPoint = Vector2.Lerp(startPos1, startPos2, Half);
                screenPoint = new Vector3(midPoint.x, midPoint.y, cameraDistance);
                // Find the distance between the two touches
                previousDistance = Vector2.Distance(startPos1, startPos2);
                couldBeZoom = true;
            }
            else
            {
                // Calculate delta distance between the two touches
                float currentDistance = Vector2.Distance(Input.touches[0].position, Input.touches[1].position);
                deltaDistance = previousDistance - currentDistance;
                previousDistance = currentDistance;
                OnZoom(deltaDistance / Screen.height);
            }
        }
        else
        {
            couldBeZoom = false;
        }
    }
    
    private void OnZoom(float amount)
    {
        zoomSystem.Result.Zoom(amount * zoomSpeed * (inverseZoom ? -1f : 1f), screenPoint);
    }
}
