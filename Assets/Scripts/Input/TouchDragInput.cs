﻿using UnityEngine;

/*
SUMMARY:
• Calls panSystem's OnBeganPan() when user starts dragging
• OnDrag() gives panSystem delta drag distance each frame
*/

public class TouchDragInput : PlatformDependentBehaviour
{
    [SerializeField] private IPanSystemContainer panSystem;
    [SerializeField] private float minDragDistance = 0.01f;
    [SerializeField] private float dragSpeed = 1f;
    private bool isDrag = false;
    private bool couldBeDrag = false;
    private Vector2 startingPosition;
    
    protected override void Awake()
    {
        base.Awake();
        minDragDistance *= Screen.height;
    }
    
    private void Update()
    {
        if (Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);
            
            if (touch.phase == TouchPhase.Began)
            {
                couldBeDrag = true;
                startingPosition = touch.position;
            }
            else if (touch.phase == TouchPhase.Moved)
            {
                if (isDrag)
                {
                    OnDrag(touch.deltaPosition / Screen.height);
                }
                else if (couldBeDrag)
                {
                    if (Vector2.Distance(touch.position, startingPosition) > minDragDistance)
                    {
                        isDrag = true;
                        panSystem.Result.OnBeganPan();
                    }
                }
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                couldBeDrag = false;
                isDrag = false;
            }
        }
        else
        {
            couldBeDrag = false;
            isDrag = false;
        }
    }
    
    protected virtual void OnDrag(Vector3 distance)
    {
        panSystem.Result.Pan(distance * dragSpeed);
    }
}
