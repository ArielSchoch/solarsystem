﻿using UnityEngine;

/*
SUMMARY:
• OnZoom() sends zoomSystem the delta zoom each frame
*/

public class MouseZoomInput : PlatformDependentBehaviour
{
    [SerializeField] private IZoomSystemContainer zoomSystem;
    [SerializeField] private float cameraDistance = 10f;
    [SerializeField] private float zoomSpeed = 5f;
    [SerializeField] private bool inverseZoom = false;
    
    private void Update()
    {
        if (Input.mouseScrollDelta.y != 0f)
            OnZoom(Input.mouseScrollDelta.y, new Vector3(Input.mousePosition.x, Input.mousePosition.y, cameraDistance));
    }
    
    private void OnZoom(float amount, Vector3 mouseScreenPoint)
    {
        zoomSystem.Result.Zoom(amount * zoomSpeed * (inverseZoom ? -1f : 1f), mouseScreenPoint);
    }
}
