﻿using UnityEngine;

/*
SUMMARY:
• Calls panSystem's OnBeganPan() when user starts dragging
• OnDrag() gives panSystem delta drag distance each frame
*/

public class MouseDragInput : PlatformDependentBehaviour
{
    [SerializeField] private IPanSystemContainer panSystem;
    [SerializeField] private float minDragDistance = 0.01f;
    [SerializeField] private float dragSpeed = 1f;
    private bool couldBeDrag = false;
    private bool isDrag = false;
    private Vector3 currentMousePosition;
    private Vector3 previousMousePosition;
    private readonly Vector3 screenSize = new Vector3(Screen.height, Screen.height, 1f);
    
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            couldBeDrag = true;
            previousMousePosition = Input.mousePosition.DivideBy(screenSize);
        }
        else if (Input.GetMouseButton(0) && couldBeDrag)
        {
            currentMousePosition = Input.mousePosition.DivideBy(screenSize);
            if (!isDrag)
            {
                if (Vector3.Distance(currentMousePosition, previousMousePosition) > minDragDistance)
                {
                    isDrag = true;
                    panSystem.Result.OnBeganPan();
                }
                else return;
            }
            
            OnDrag(previousMousePosition - currentMousePosition);
            previousMousePosition = currentMousePosition;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            couldBeDrag = false;
            isDrag = false;
        }
    }
    
    protected virtual void OnDrag(Vector3 distance)
    {
        panSystem.Result.Pan(distance * dragSpeed);
    }
}
