﻿using Zenject;

public class SolarSystemInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        Container.Bind<SelectionEventMediator>().AsSingle();
        Container.Bind<ITimeScaleProvider>().To<GameTimeScale>()
                 .FromComponentInNewPrefabResource("Managers/GameManager").AsSingle();
    }
}
