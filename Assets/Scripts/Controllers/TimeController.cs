﻿using UnityEngine;
using Zenject;

public class TimeController : MonoBehaviour
{
    private ITimeScaleProvider timeScaleProdiver;

    [Inject]
    public void Init(ITimeScaleProvider timeScaleProdiver)
    {
        this.timeScaleProdiver = timeScaleProdiver;
    }

    public void DoubleTime()
    {
        timeScaleProdiver.TimeScale *= 2f;
    }

    public void HalfTime()
    {
        timeScaleProdiver.TimeScale /= 2f;
    }
}
