﻿using UnityEngine;
using Zenject;

public class SpaceBodyController : MonoBehaviour, ISelectable
{
    public Transform Transform { get { return this.transform; } }
    public float FocusDistance { get { return focusDistance; } }
    [SerializeField] private float focusDistance = 1f;
    [SerializeField] private OrbitSystem orbitSystem;
    [SerializeField] private GameObject orbitRendererPrefab;
    private SelectionEventMediator selectionEventMediator;

    [Inject]
    public void Init(SelectionEventMediator selectionEventMediator)
    {
        this.selectionEventMediator = selectionEventMediator;
    }

    private void OnEnable()
    {
        selectionEventMediator.Selected += OnSelectedTarget;
    }

    private void OnDisable()
    {
        selectionEventMediator.Selected -= OnSelectedTarget;
    }

    protected virtual void OnSelectedTarget(object sender, SelectionEventArgs e)
    {
        if (e.target.Result.Transform != this.transform) return;
        ShowOrbit();
    }

    private void ShowOrbit()
    {
        var orbitRenderers = GameObject.FindObjectsOfType<OrbitRenderSystem>();
        OrbitRenderSystem orbitRenderer;

        if (orbitSystem != null)
        {
            if (orbitRenderers.Length == 0)
            {
                var renderObject = GameObject.Instantiate(orbitRendererPrefab, orbitSystem.transform.position, Quaternion.identity) as GameObject;
                orbitRenderer = renderObject.GetComponent<OrbitRenderSystem>();
            }
            else orbitRenderer = orbitRenderers[0];

            orbitRenderer.Initialize(orbitSystem);
            orbitRenderer.RenderOrbit();
        }
    }
}
