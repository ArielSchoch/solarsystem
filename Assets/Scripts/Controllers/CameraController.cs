﻿using System;
using UnityEngine;
using Zenject;

/*
SUMMARY:
• The controller that ties together the components of the camera
• Subscribes to the events OnSelectedTarget, OnFocusedOnTarget and OnBeganPan
*/

public class CameraController : MonoBehaviour
{
    [SerializeField] private ITargetFollowSystemContainer targetFollowSystem;
    [SerializeField] private ITargetFocusSystemContainer targetFocusSystem;
    [SerializeField] private IPanSystemContainer panSystem;
    private SelectionEventMediator selectionEventMediator;
    private ISelectable target;

    [Inject]
    public void Init(SelectionEventMediator selectionEventMediator)
    {
        this.selectionEventMediator = selectionEventMediator;
    }

    private void OnEnable()
    {
        selectionEventMediator.Selected += OnSelectedTarget;
        targetFocusSystem.Result.Focused += OnFocusedOnTarget;
        panSystem.Result.BeganPan += OnBeganPan;
    }

    private void OnDisable()
    {
        selectionEventMediator.Selected -= OnSelectedTarget;
        targetFocusSystem.Result.Focused -= OnFocusedOnTarget;
        panSystem.Result.BeganPan -= OnBeganPan;
    }

    protected virtual void OnSelectedTarget(object sender, SelectionEventArgs e)
    {
        target = e.target.Result;
        if (targetFollowSystem != null)
            targetFollowSystem.Result.FollowTarget = false;
        targetFocusSystem.Result.FocusOnTarget(target);
    }

    protected virtual void OnFocusedOnTarget(object sender, SelectionEventArgs e)
    {
        if (targetFollowSystem != null)
        {
            targetFollowSystem.Result.Target = target.Transform;
            targetFollowSystem.Result.FollowTarget = true;
        }
    }

    protected virtual void OnBeganPan(object sender, EventArgs e)
    {
        targetFollowSystem.Result.FollowTarget = false;
    }
}
