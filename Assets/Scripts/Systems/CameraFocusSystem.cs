﻿using UnityEngine;
using System;

/*
SUMMARY:
• FocusOnTarget() externally invoked (by CameraController)
• Focused event invoked once script has finished focusing (subscribed to by CameraController)
*/

[RequireComponent(typeof(Camera))]
public class CameraFocusSystem : MonoBehaviour, ITargetFocusSystem
{
    public event EventHandler<SelectionEventArgs> Focused;
    [SerializeField] private float focusSpeed = 1f;
    [SerializeField] private bool moveX = true;
    [SerializeField] private bool moveY = true;
    [SerializeField] private bool moveZ = true;
    [SerializeField] private LeanTweenType moveEaseType;
    [SerializeField] private LeanTweenType zoomEaseType;
    private new Camera camera;
    
    private void Awake()
    {
        camera = this.GetComponent<Camera>();
    }
    
    public void FocusOnTarget(ISelectable target)
    {
        // Store starting position
        var originalPosition = this.transform.position;
        
        // Move camera to target
        LeanTween.move(this.gameObject, target.Transform, focusSpeed).setEase(moveEaseType)
                 .setOnUpdate((float v) =>
                 {
                     var pos = this.transform.position;
                     if (!moveX)
                         pos.x = originalPosition.x;
                     if (!moveY)
                         pos.y = originalPosition.y;
                     if (!moveZ)
                         pos.z = originalPosition.z;
                     this.transform.position = pos;
                 });
        
        // Zoom in on target
        LeanTween.value(this.gameObject, camera.orthographicSize, target.FocusDistance, focusSpeed)
                 .setOnUpdate(v => { camera.orthographicSize = v; })
                 .setOnComplete(() => { if (Focused != null) Focused(this, new SelectionEventArgs() { target = new ISelectableContainer() { Result = target } }); })
                 .setEase(zoomEaseType);
    }
}
