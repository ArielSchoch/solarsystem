﻿using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraZoomSystem : MonoBehaviour, IZoomSystem
{
    [SerializeField] private bool canZoom = true;
    [SerializeField] private float minZoom = 40f;
    [SerializeField] private float maxZoom = 1f;
    [SerializeField] private float zoomSpeed = 1f;
    private new Camera camera;
    
    private void Awake()
    {
        camera = this.GetComponent<Camera>();
    }
    
    public void Zoom(float zoomAmount, Vector3 screenPoint)
    {
        if (!canZoom) return;
        // Store the world point at screen point before zooming
        Vector3 previousWorldPoint = camera.ScreenToWorldPoint(screenPoint);
        // Change the camera's zoom
        camera.orthographicSize = Mathf.Clamp(camera.orthographicSize + (zoomAmount * zoomSpeed * (camera.orthographicSize / minZoom)), maxZoom, minZoom);
        // Adjust the camera's position to keep the world space and screen space zoom coordinates aligned
        this.transform.position += previousWorldPoint - camera.ScreenToWorldPoint(screenPoint);
    }
}
