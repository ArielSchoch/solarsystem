﻿using UnityEngine;

public class FixedRotationSystem : MonoBehaviour
{
    [SerializeField] private IOrbitProviderContainer orbitProvider;
    [SerializeField] private float deltaCompletion = 0.001f;

    private void Update()
    {
        Vector3 nextPos = orbitProvider.Result.Orbit(orbitProvider.Result.OrbitCompletion + deltaCompletion);
        this.transform.up = this.transform.position - nextPos;
    }
}
