﻿using System;
using UnityEngine;

[Serializable]
public class OriginDistance
{
    public float radius;
    public Vector3 origin;
}

/*
SUMMARY:
• OnBeganPan() gets called externally (by DragInput)
• BeganPan event gets invoked (CameraController listens in order to disable FollowTargetSystem)
• Pan() gets called externally (by DragInput) and is clamped to bounds
*/

[RequireComponent(typeof(Camera))]
public class CameraPanSystem : MonoBehaviour, IPanSystem
{
    private const float CameraToWorldSize = 2f;
    private const float EpsilonPanVelocity = 0.000001f;
    public event EventHandler BeganPan;
    [SerializeField] private float panSpeed = 1f;
    [SerializeField] private bool scaleDragSpeed = true;
    [SerializeField] private OriginDistance circleBounds;
    [SerializeField] private float stretchVelocity = 1f;
    [SerializeField] private float panDrag = 1f;
    private new Camera camera;
    private Vector2 panVelocity;

    private void Awake()
    {
        if (scaleDragSpeed)
        {
            camera = this.GetComponent<Camera>();
            if (camera == null)
                scaleDragSpeed = false;
        }
    }

    private void LateUpdate()
    {
        Vector2 direction = (circleBounds.origin - this.transform.position).normalized;
        var distanceOutOfBounds = Mathf.Max(0f, this.transform.position.magnitude - circleBounds.radius);
        panVelocity += direction * distanceOutOfBounds * stretchVelocity * Time.deltaTime;

        if (panVelocity.sqrMagnitude > 0f)
        {
            panVelocity -= panVelocity * panDrag * Time.deltaTime;
            
            if (panVelocity.sqrMagnitude > EpsilonPanVelocity)
                this.transform.position = this.transform.position + (Vector3) panVelocity;
            else
                panVelocity = Vector2.zero;
        }
    }

    public void OnBeganPan()
    {
        if (BeganPan != null)
            BeganPan(this, EventArgs.Empty);
    }

    public void Pan(Vector2 direction)
    {
        panVelocity = direction * panSpeed * (scaleDragSpeed ? camera.orthographicSize * CameraToWorldSize : 1f);
    }
}
