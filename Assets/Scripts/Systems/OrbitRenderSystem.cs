﻿using System;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class OrbitRenderSystem : MonoBehaviour
{
    [SerializeField] private new Camera camera;
    [SerializeField] private ITargetFollowSystemContainer targetFollowSystem;
    [SerializeField] private IOrbitProvider orbitProvider;
    [SerializeField] private int divisions = 60;
    [SerializeField] private Color fadeToColor;
    [SerializeField] private float fadeDuration = 0.5f;
    [SerializeField] private float width = 0.07f;
    [SerializeField] private bool scaleWithZoom = true;
    [SerializeField] private float minWidth = 0.03f;
    [SerializeField] private float maxWidth = 0.1f;
    [SerializeField] private float minZoom = 5f;
    [SerializeField] private float maxZoom = 40f;
    private LineRenderer lineRenderer;
    private bool renderOrbit = false;

    private void Awake()
    {
        lineRenderer = this.GetComponent<LineRenderer>();
        if (camera == null)
            camera = Camera.main;
    }

    private void Update()
    {
        if (renderOrbit && orbitProvider != null)
            for (int i = 0; i <= divisions; i++)
                lineRenderer.SetPosition(i, orbitProvider.Orbit(i / (float) divisions));

        if (!scaleWithZoom) return;

        float scaledWidth = Mathf.Lerp(minWidth, maxWidth, Mathf.InverseLerp(minZoom, maxZoom, camera.orthographicSize));
        lineRenderer.startWidth = scaledWidth;
        lineRenderer.endWidth = scaledWidth;
    }

    public void Initialize(IOrbitProvider orbitProvider)
    {
        this.orbitProvider = orbitProvider;
    }

    public void RenderOrbit()
    {
        renderOrbit = true;

        lineRenderer.positionCount = divisions + 1;
        if (!scaleWithZoom)
        {
            lineRenderer.startWidth = width;
            lineRenderer.endWidth = width;
        }

        for (int i = 0; i <= divisions; i++)
            lineRenderer.SetPosition(i, orbitProvider.Orbit(i / (float) divisions));
        
        Action<Color> colorCallback = c => { lineRenderer.startColor = c; lineRenderer.endColor = c; };
        LeanTween.value(this.gameObject, colorCallback, new Color(0f, 0f, 0f, 0f), fadeToColor, fadeDuration);

        targetFollowSystem.Result.Target = orbitProvider.OrbitTarget;
        targetFollowSystem.Result.FollowTarget = true;
    }
}
