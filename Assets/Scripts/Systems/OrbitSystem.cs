﻿using System;
using UnityEngine;
using Zenject;

public class MovementEventArgs : System.EventArgs
{
    public Vector3 position;
}

// Potentially redundant interface since only OrbitSystem implements it
public interface IOrbitProvider
{
    float OrbitRotation { get; }
    Transform OrbitTarget { get; }
    float OrbitCompletion { get; }
    Vector3 Orbit(float completion);
}

[Serializable]
public class IOrbitProviderContainer : IUnifiedContainer<IOrbitProvider> {}

public class OrbitSystem : MonoBehaviour, IMovementNotifier, IOrbitProvider
{
    public float OrbitRotation { get { return orbitRotation; } }
    public Transform OrbitTarget { get { return ((Component) target.Object).transform; } }
    public float OrbitCompletion { get { return orbitCompletion; } }

    private const float PerMinute = 60f;
    public event EventHandler<MovementEventArgs> OnMoved;
    [SerializeField] private IMovementNotifierContainer target;
    [SerializeField] private bool orbit = true;
    [SerializeField] private Vector3 orbitOffset;
    [SerializeField] private Vector2 orbitWidth = Vector2.one;
    [SerializeField] private float orbitsPerMinute = 1f;
    [SerializeField] private float orbitCompletion = 0f;
    [SerializeField] private float orbitRotation = 0f;
    [SerializeField] private float orbitRotationSpeed = 0f;
    [SerializeField] private float orbitDistance = 1f;
    private ITimeScaleProvider timeScaleProdiver;
    private MovementEventArgs eventArgs = new MovementEventArgs();
    private float orbitSpeed;

    [Inject]
    public void Init(ITimeScaleProvider timeScaleProdiver)
    {
        this.timeScaleProdiver = timeScaleProdiver;
    }

    private void Awake()
    {
        orbitSpeed = orbitsPerMinute / PerMinute;
    }

    private void OnEnable()
    {
        if (target.Result != null)
            target.Result.OnMoved += OnOrbit;
    }

    private void OnDisable()
    {
        if (target.Result != null)
            target.Result.OnMoved -= OnOrbit;
    }

    // Send zero position event if we haven't set a target
    private void Update()
    {  
        if (target.Result != null) return;

        eventArgs.position = this.transform.position;
        OnMoved(this, eventArgs);
    }

    // If we have set a target and/or we want to orbit
    protected virtual void OnOrbit(object sender, MovementEventArgs e)
    {
        if (orbit)
        {
            // Add optional swing to orbit
            orbitRotation = (orbitRotation + orbitRotationSpeed * timeScaleProdiver.TimeScale * Time.deltaTime) % 1;
            // Rotate this object around target
            orbitCompletion = (orbitCompletion + orbitSpeed * timeScaleProdiver.TimeScale * Time.deltaTime) % 1;
            this.transform.position = e.position + Orbit(orbitCompletion);
        }

        if (OnMoved == null) return;

        // Notify subscribers that this object has moved
        eventArgs.position = this.transform.position;
        OnMoved(this, eventArgs);
    }

    public Vector3 Orbit(float completion)
    {
        var c = Mathf.Lerp(0f, 2 * Mathf.PI, completion);
        var x = Mathf.Sin(c) * orbitWidth.x * -1f;
        var y = Mathf.Cos(c) * orbitWidth.y;
        return (orbitOffset + new Vector3(x, y) * orbitDistance).Rotate(orbitRotation);
    }
}
