﻿using UnityEngine;

public class LookAtSystem : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private bool lookAtTarget;
    
    private void LateUpdate()
    {
        if (!lookAtTarget) return;
        
        Vector3 dir = target.position - this.transform.position;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        
         this.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }
}
