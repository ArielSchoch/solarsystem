﻿using UnityEngine;

/*
SUMMARY:
• Ensures planet colliders remain a clickable size when zoomed out
*/

[RequireComponent(typeof(CircleCollider2D))]
public class ColliderScaleSystem : MonoBehaviour
{
    [SerializeField] private new Camera camera;
    [SerializeField] private float minRadius = 0.5f;
    [SerializeField] private float maxRadius = 2f;
    [SerializeField] private float maxZoom = 10f;
    [SerializeField] private float minZoom = 40f;
    private CircleCollider2D circleCollider2D;

	private void Awake()
	{
		circleCollider2D = this.GetComponent<CircleCollider2D>();
	}

    private void Update()
    {
        if (camera == null) return;
        var value = Mathf.InverseLerp(maxZoom, minZoom, camera.orthographicSize);
        circleCollider2D.radius = Mathf.Lerp(minRadius, maxRadius, value);
    }
}
