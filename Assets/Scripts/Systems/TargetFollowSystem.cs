﻿using UnityEngine;

public class TargetFollowSystem : MonoBehaviour, ITargetFollowSystem
{
    public Transform Target { get { return target; } set { target = value; } }
    public bool FollowTarget { get { return followTarget; } set { followTarget = value; } }
    [SerializeField] private Transform target;
    [SerializeField] private Vector3 offset;
    [SerializeField] private bool followTarget;
    [SerializeField] private bool followX;
    [SerializeField] private bool followY;
    [SerializeField] private bool followZ;
    
    private void LateUpdate()
    {
        if (!followTarget || Target == null) return;
        
        var posX = followX ? Target.position.x : this.transform.position.x;
        var posY = followY ? Target.position.y : this.transform.position.y;
        var posZ = followZ ? Target.position.z : this.transform.position.z;
        
        this.transform.position = offset + new Vector3(posX, posY, posZ);
    }
}
