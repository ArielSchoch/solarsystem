﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

[Serializable]
public class SelectionEventArgs : EventArgs
{
    public ISelectableContainer target;
}

/*
SUMMARY:
• Implements Unity's IPointerUp and IPointerClick handlers
• Receives an SelectionEventMediator as a dependency
• Calls SelectionEventMediator's OnSelected() method
*/

public class SelectionEventSystem : MonoBehaviour, IPointerUpHandler, IPointerClickHandler
{
    public SelectionEventArgs eventArgs;
    private SelectionEventMediator selectionEventMediator;
    
    [Inject]
    public void Init(SelectionEventMediator selectionEventMediator)
    {
        this.selectionEventMediator = selectionEventMediator;
    }
    
    public void OnPointerClick(PointerEventData eventData)
    {
        #if !UNITY_EDITOR
        if (Input.touchCount == 1)
        #endif
        OnSelected();
    }
    
    public void OnPointerUp(PointerEventData eventData) {}
    
    protected virtual void OnSelected()
    {
        selectionEventMediator.OnSelected(this, eventArgs);
    }
}
