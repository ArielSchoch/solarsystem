﻿using UnityEngine;
using Zenject;

[RequireComponent(typeof(Rigidbody2D))]
public class RotationSystem : MonoBehaviour
{
    private const float PerMinute = 60f;
    private const float FullRotation = 360f;
    [SerializeField] private float rotationsPerMinute;
    private ITimeScaleProvider timeScaleProdiver;
    private new Rigidbody2D rigidbody2D;
    private float rotateSpeed;

    [Inject]
    public void Init(ITimeScaleProvider timeScaleProdiver)
    {
        this.timeScaleProdiver = timeScaleProdiver;
    }

    private void Awake()
    {
        rigidbody2D = this.GetComponent<Rigidbody2D>();
        rotateSpeed = FullRotation * rotationsPerMinute / PerMinute;
    }

    private void Update()
    {
        rigidbody2D.angularVelocity = rotateSpeed * timeScaleProdiver.TimeScale;
    }
}
