﻿using UnityEngine;

public static class ExtensionMethods
{
    public static Vector2 DivideBy(this Vector2 v1, Vector2 v2)
    {
        v1.x /= v2.x;
        v1.y /= v2.y;
        return v1;
    }
    
    public static Vector3 DivideBy(this Vector3 v1, Vector3 v2)
    {
        v1.x /= v2.x;
        v1.y /= v2.y;
        v1.z /= v2.z;
        return v1;
    }

    public static Vector2 Rotate(this Vector2 v, float degrees)
    {
        return v.RotateRadians(Mathf.Lerp(0f, 2 * Mathf.PI, degrees));
    }

    public static Vector2 RotateRadians(this Vector2 v, float radians)
    {
        var ca = Mathf.Cos(radians);
        var sa = Mathf.Sin(radians);
        return new Vector2(ca * v.x - sa * v.y, sa * v.x + ca * v.y);
    }

    public static Vector3 Rotate(this Vector3 v, float degrees)
    {
        return v.RotateRadians(Mathf.Lerp(0f, 2 * Mathf.PI, degrees));
    }

    public static Vector3 RotateRadians(this Vector3 v, float radians)
    {
        var ca = Mathf.Cos(radians);
        var sa = Mathf.Sin(radians);
        return new Vector3(ca * v.x - sa * v.y, sa * v.x + ca * v.y, v.z);
    }
}
