﻿using UnityEngine;

public class PlatformDependentBehaviour : MonoBehaviour
{
    public bool isMobile;
    
    protected virtual void Awake()
    {
        // If app is running on a mobile device
        #if !UNITY_EDITOR && (UNITY_IOS || UNITY_ANDROID)
        // and this isn't a mobile component; remove it.
        if (!isMobile) Destroy(this);
        #else
        // if app is running in editor (or similar) and this is a mobile component then remove it.
        // Additionally remove non mobile components if we are testing using the Unity Remote app.
            #if REMOTE
            if (!isMobile) Destroy(this);
            #else
            if (isMobile) Destroy(this);
            #endif
        #endif
    }
}
