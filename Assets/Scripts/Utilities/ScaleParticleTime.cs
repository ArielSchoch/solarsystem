﻿using System;
using UnityEngine;
using Zenject;

[RequireComponent(typeof(ParticleSystem))]
public class ScaleParticleTime : MonoBehaviour
{
    [SerializeField] private float lifeTime = 5f;
    private ITimeScaleProvider timeScaleProdiver;
    private new ParticleSystem particleSystem;

    [Inject]
    public void Init(ITimeScaleProvider timeScaleProdiver)
    {
        this.timeScaleProdiver = timeScaleProdiver;
        timeScaleProdiver.Changed += OnChangedTimeScale;
    }

    private void Awake()
    {
        particleSystem = this.GetComponent<ParticleSystem>();
    }
    
    private void Disable()
    {
        timeScaleProdiver.Changed -= OnChangedTimeScale;
    }

    protected virtual void OnChangedTimeScale(object sender, EventArgs e)
    {
        var main = particleSystem.main;
        main.startLifetime = lifeTime / timeScaleProdiver.TimeScale;
    }
}
