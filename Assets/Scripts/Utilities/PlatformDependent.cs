﻿using System;
using UnityEngine;

[Serializable]
public class PlatformDependent<T> where T : class
{
    [SerializeField] private T desktopInstance;
    [SerializeField] private T mobileInstance;
    
    public T Instance
    {
        get
        {
            #if !UNITY_EDITOR && (UNITY_IOS || UNITY_ANDROID)
            return mobileInstance;
            #else
            if (UnityEditor.EditorApplication.isRemoteConnected)
                return mobileInstance;
            else
                return desktopInstance;
            #endif
        }
    }
}