﻿using System;

/*
SUMMARY:
• Groups all selection events into a single script to which one can subscribe
*/

public class SelectionEventMediator
{
    public event EventHandler<SelectionEventArgs> Selected;

    public virtual void OnSelected(object sender, SelectionEventArgs e)
    {
        if (Selected != null)
            Selected(sender, e);
    }
}
