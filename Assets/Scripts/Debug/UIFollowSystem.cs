﻿using UnityEngine;

public class UIFollowSystem : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private new Camera camera;
    [SerializeField] private Vector3 offset;

    // FIX: Code being run more often than it needs to.
    private void OnGUI()
    {
        if (target != null)
            this.transform.position = camera.WorldToScreenPoint(target.position + offset);
    }
}
