﻿using UnityEngine;

public class DistanceLogger : MonoBehaviour
{
    [SerializeField] private Transform target;

    private void Update()
    {
        Debug.Log("Distance: " + Vector3.Distance(this.transform.position, target.position));
    }
}
