﻿using System;

[Serializable]
public class PopulationCountData : SerializableData<PopulationCountData>
{
    public int populationCount = 1000;
    
    public PopulationCountData(string objectName) : base(objectName) {}
    
    protected override void AssignValues(PopulationCountData loaded)
    {
        populationCount = loaded.populationCount;
    }
}
