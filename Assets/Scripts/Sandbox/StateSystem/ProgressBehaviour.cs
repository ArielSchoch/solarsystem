﻿using UnityEngine;
using System.Collections;
using System;

public class ProgressBehaviour : MonoBehaviour
{
    public string objectName;
    [HideInInspector] public SpaceObjectData spaceObjectData;
    [HideInInspector] public PopulationCountData populationCountData;

    protected virtual void Awake()
    {
        InitializeSerializableData();
        
        Debug.Log("Loaded mass: " + spaceObjectData.mass);
        Debug.Log("Loaded populationCount: " + populationCountData.populationCount);
    }
    
    protected virtual void InitializeSerializableData()
    {
        spaceObjectData = new SpaceObjectData(objectName);
        populationCountData = new PopulationCountData(objectName);
    }
    
    protected virtual void Update()
    {
        populationCountData.populationCount++;
    }
    
    protected virtual void OnApplicationQuit()
    {
        populationCountData.Save();
    }
}
