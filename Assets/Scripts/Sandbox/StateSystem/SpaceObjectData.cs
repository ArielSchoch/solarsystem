﻿using System;

[Serializable]
public class SpaceObjectData : SerializableData<SpaceObjectData>
{
    public float mass = 1f;
    
    public SpaceObjectData(string objectName) : base(objectName) {}

    protected override void AssignValues(SpaceObjectData loaded)
    {
        mass = loaded.mass;
    }
}
