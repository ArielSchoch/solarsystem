﻿using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

[Serializable]
public abstract class SerializableData<T> where T : class
{
    public string objectName;
    public string SerializablePath { get { return objectName + "_" + this.GetType().ToString(); } }
    
    public SerializableData(string objectName)
    {
        this.objectName = objectName;
        Load();
    }
    
    public virtual void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(GetFilePath(), FileMode.Create);
        bf.Serialize(file, this);
        file.Close();
    }

    public virtual void Load()
    {
        if(File.Exists(GetFilePath()))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(GetFilePath(), FileMode.Open);
            T loaded = bf.Deserialize(file) as T;
            file.Close();
            AssignValues(loaded);
        }
    }
    
    protected abstract void AssignValues(T loaded);

    public virtual string GetFilePath()
    {
        return Application.persistentDataPath + "/" + SerializablePath + ".dat";
    }
}